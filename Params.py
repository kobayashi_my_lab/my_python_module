
import copy
import os
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import StratifiedKFold,KFold
from sklearn.model_selection import GridSearchCV,RandomizedSearchCV
from sklearn.metrics import mean_squared_error,mean_absolute_error,accuracy_score,roc_auc_score,roc_curve
from sklearn.preprocessing import StandardScaler

from my_python_module.path_setting import PROCESSED_ROOT

class BaseModel(object):
    init_params = None
    add_params = None
    Model = None
    scaling = None
    def __init__(self,filename,add_init_param = None,add_params_search = None,cv = 5,search_param_cv = 5):

        self.file = filename
        self.filename = os.path.join(PROCESSED_ROOT,"data",filename)
        if os.path.exists(self.filename) is not True:
            os.makedirs(self.filename,exist_ok=True)

        self.cv = cv
        self.search_params = search_param_cv

        self._init_params = copy.deepcopy(self.init_params)

        if add_init_param is not None:

            self._init_params.update(add_init_param)

        if add_params_search is not None:

            self.add_params.update(add_params_search)


        #パラメータサーチするためのリストを作る
        self.search_params = [ParamsModel(init_params = self._init_params,
                                          add_params = self.add_params,
                                          filename = self.filename,
                                          search_param_cv = search_param_cv,
                                          Model = self.Model,
                                          prepend_name = i,
                                          scaling = self.scaling)
                              for i in range(self.cv)]


    def metric(self,predict,y_true,eval,i,columns = None):
        if eval == "clf":
            auc = roc_auc_score(y_true,predict)
            predict = np.where(predict > 0.5,1,0)
            acc = accuracy_score(y_true,predict)

            #ROC曲線を描く
            fpr,tpr,threshholds = roc_curve(y_true,predict)
            plt.plot(fpr,tpr,label = "ROC curve:(area = {:.2f})".format(auc))
            plt.xlabel("Negative rate")
            plt.ylabel("Positive rate")
            plt.grid()
            plt.tight_layout()
            plt.savefig(os.path.join(self.filename,"ROC.png"))
            plt.close()


            predict = pd.DataFrame([[auc,acc]],columns=["auc","acc"])
            path = os.path.join(self.filename,"{}_clf_score.csv".format(i))
            predict.to_csv(path,index=False)
            return acc

        elif eval == "reg":
            mean_square = mean_squared_error(y_true,predict)
            mean_absolute = mean_absolute_error(y_true,predict)

            predict = pd.DataFrame([[mean_square,mean_absolute]],columns=["mean_square","mean_absolute"])
            path = os.path.join(self.filename,"{}_reg_score.csv".format(i))
            predict.to_csv(path,index=False)
            return mean_square

    def train_save(self,test_predict):
        name = self.file + "_predict"
        test_predict = pd.DataFrame(test_predict, columns=[name])
        path = os.path.join(self.filename, "test.csv")
        test_predict.to_csv(path, index=False)
        return test_predict


    def fit(self,df,y,test = None,eval = "clf"):

        #存在するときのpathを追加しないと・・。。
        test_path = os.path.join(self.filename,"test.csv")
        train_path = os.path.join(self.filename,"train_{}.csv".format(self.file))
        if os.path.exists(train_path) and os.path.exists(test_path):
            print("訓練データを読み込みました")
            train = pd.read_csv(train_path)
            test = pd.read_csv(test_path)

            return train,test

        columns = df.columns

        if test is not None:
            test = test.values
        train = df.values

        folds = None
        if eval == "reg":
            folds = KFold(n_splits=self.cv,shuffle=True,random_state=777)
        elif eval == "clf":
            folds = StratifiedKFold(n_splits=self.cv,random_state=777)

        eval_predict = 0
        eval_avg = 0
        test_predict = 0

        train_oneof = np.zeros(train.shape[0])
        for i,(train_idx,eval_idx) in enumerate(folds.split(train,y)):

            print("Model try {}/{}".format(i+1,self.cv))
            X_train,X_eval = train[train_idx],train[eval_idx]
            y_train,y_eval = y[train_idx],y[eval_idx]

            Model = self.fit_model(X_train,X_eval,y_train,y_eval,i)

            if self.scaling is not None:
                test = self.search_params[i].scaler.transform(test)

            if eval == "clf":
                eval_predict = Model.predict_proba(X_eval)[:,1]
                if test is not None:
                  test_predict += Model.predict_proba(test)[:,1]

            elif eval == "reg":
                eval_predict = Model.predict(X_eval)
                if test is not None:
                  test_predict += Model.predict(test)

            #one-of-hot
            train_oneof[eval_idx] = eval_predict
            #score　評価
            eval_avg += self.metric(predict=eval_predict,y_true=y_eval,eval = eval,i=i,columns = columns)

        eval_avg = pd.DataFrame([eval_avg/self.cv],columns=["eval_avg"])
        eval_avg.to_csv(os.path.join(self.filename,"avg_score.csv"),index=False)

        train_oneof = pd.DataFrame(train_oneof,columns=["train_{}".format(self.file)])
        train_oneof.to_csv(os.path.join(self.filename,"train_{}.csv".format(self.file)),index=False)


        if test is not None:
            test_predict /= self.cv
            test_predict = self.train_save(test_predict)

        return train_oneof,test_predict





    #継承するときにかきかえられるようにする
    def fit_model(self,X_train,X_eval,y_train,y_eval,i):
        Model = self.search_params[i].fit(X_train,y_train)
        return Model





class ParamsModel(object):
    def __init__(self,init_params,add_params,filename,Model
                 ,prepend_name,search_param_cv = 5,scaling = None):

        """

        :param init_params: デフォルトで変更しないparam
        :param add_params: サーチしたいparam
        :param filename: 保存するディレクトリ
        :param Model: 使用するモデル
        :param prepend_name: jsonファイルで書き出すときに区別する
        :param search_param_cv: searchするときのCV
        :param scaling: None
        """

        self.init_params = init_params
        self.add_params = add_params
        self.filename = filename
        self.search_param_cv = search_param_cv
        self.Model = Model
        self.best_model = None
        self.best_score = None
        self.best_param = copy.deepcopy(self.init_params)
        self.prepend_name = prepend_name
        self.scaling = scaling
        self.scaler = None




    @property
    def get_Model(self):
        print(self.init_params)
        return self.Model(**self.init_params)

    def save_model(self):
        param_path = os.path.join(self.filename, '{}_best_param.json'.format(self.prepend_name))
        with open(param_path, 'w') as f:
            json.dump(self.best_param, f, sort_keys=True, indent=4)


    def fit(self,X_train,y_train,**kwargs):

        if self.scaling is not None:
            scaler = StandardScaler().fit(X_train)
            X_train = scaler.transform(X_train)
            self.scaler = scaler


        if self.add_params is None:
            self.best_model = self.get_Model.fit(X_train,y_train,**kwargs)
        else:
            self.best_model = self.search_param(model = self.get_Model,x = X_train,y = y_train,
                                                param_distribution=self.add_params,fit_params=kwargs)

        self.save_model()

        return self.best_model

        # self.save_model()
    def search_param(self, model, x, y,
                         param_distribution=None,
                         fit_params=None,how = "random"):
        """
                パラメータ候補から cv による score の算出を行うメソッド
                既存のスコアを上回った場合のみ best_params を上書きする

                :param np.ndarray x: shape = (n_samples, n_features)
                :param np.ndarray y: shape = (n_samples, )
                :param dict param_distribution:
                    パラメータ検索の分布. パラメータ名を key, 候補の配列を value にもつ dict を渡す.
                :param float scale_pos_weight:
                :param str how: 検索方法. `"random"` or `"grid"` を指定する
                :param int n_jobs: cv を行う CPU の数
                :param bool iid:
                :return: 学習済みの model
                """
        common_settings = dict(

            cv=KFold(n_splits=self.search_param_cv, random_state=777),
            # scoring で dict が与えられている時は refit は名前を指定する必要が
            # TODO: scoring が dict かどうか判断して refit に渡すパラメータを考えるようにする
        )

        #XGBoostなどではここにparameterが付与される。
        if fit_params is not None:
            common_settings['fit_params'] = fit_params

        # 探索空間が search 回数を下回っているときに random search を呼び出すとエラーになるため
        # 事前に空間の数を計算して回避する
        total_space = np.prod([len(x) for x in param_distribution.values()])
        if total_space < 40:
            how = 'grid'

        # self.logger.info('search params: ' + '-'.join(param_distribution.keys()))

        if how == "random":
            # self.logger.debug('run random search')
            # note: random state は固定値
            searcher = RandomizedSearchCV(model, param_distributions=param_distribution, n_iter=40,
                                          random_state=777,
                                          **common_settings)

        elif how == "grid":
            searcher = GridSearchCV(model, param_grid=param_distribution, **common_settings)
        else:
            raise ValueError('`how` is in `"grid"` or `"random"`. actually: {}'.format(how))

        searcher.fit(x, y)

        # self.logger.info('best params: {}'.format(searcher.best_params_))
        if self.best_score is not None and searcher.best_score_ - self.best_score < 0:
            return searcher

        # self.logger.info('update parameter: {}'.format(searcher.best_params_))

        # if self.best_score:
        # self.logger.info('score: {0:.4f} -> {1:.4f}: gain: {2:.4f}'.format(
        #     self.best_score,
        #     searcher.best_score_,
        #     searcher.best_score_ - self.best_score
        # ))
        # else:
        # self.logger.info('best score: {:.4f}'.format(searcher.best_score_))
        self.best_score = searcher.best_score_
        self.best_param.update(searcher.best_params_)
        return searcher



