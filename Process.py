import os,sys
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from my_python_module.path_setting import *

class DataProcess(object):
    def __init__(self,df=None,dir = None):


        """
        一般的なデータの前処理を行うためのクラス

        :param df: データ
        :param dir: ディレクトリからデータを読み込むか保存先
        """


        self.df = df
        self.dir = dir
        #参照するためのファイル
        self.filename = None

        if self.dir is not None:
            if os.path.exists(self.dir):
                self.df = pd.read_csv(self.dir)

            else:
                self.dir = PROCESSED_ROOT
                raise ("指定されたファイルがない")


    @property
    def DataFrame(self):
        return self.df

    @property
    def visualize(self):
        path = os.path.join(PROCESSED_ROOT, "plot")
        if not os.path.exists(path):
            os.makedirs(path, exist_ok=True)
            print("ディレクトリがないため作りました。")
        return path



    def data_show(self):
        print("-----data information-----")
        print(self.DataFrame.info())
        print("-----data 5 Top-----")
        print(self.DataFrame.head(5))
        print("-----data describe-----")
        print(self.DataFrame.describe())
        print("-----data isna sum-----")
        print(self.DataFrame.isnull().sum())
        print("-----data unique------")
        print(self.DataFrame.nunique())
        print("-----data shape-----")
        print(self.DataFrame.shape)


    #outlinear
    def outlinear(self,columns,method = None,name = "outlinear",plot = None):


        """

        :param columns: 異常値があるかを調べたいとき
        :param method: 異常値を検知したインデックスを消去する方法。デフォルトNone
        :return:
        """

        if plot is not None:
            path = self.visualize


            for i,col in enumerate(columns):
                fig = plt.figure(figsize=(10,10))
                ax = fig.add_subplot(111)
                bins = int(np.log2(len(self.DataFrame[col])) + 1)
                sns.distplot(self.DataFrame[col],ax = ax,bins = bins,label=["KDE"])
                plt.title(col)
                plt.tight_layout()
                fig.savefig(os.path.join(self.visualize, "{}_".format(col)+name+".png"), dpi=150)
                plt.xlabel(col,fontsize = 14)
                plt.close(fig)

            if method is "IQR":
                Q1,Q3 = np.percentile(self.DataFrame[col],[25,75])
                IQR = Q3-Q1
                above_Q = Q1-1.5 * IQR
                bottom_Q = Q3 + 1.5*IQR

                index = self.DataFrame[col][(self.DataFrame[col] < bottom_Q) and (self.DataFrame[col] > above_Q)].index




    def save(self,df,filename,reference = None):
        if self.dir is None:
            self.dir = PROCESSED_ROOT
        if reference is not None:
            self.filename = filename
        os.makedirs(self.dir,exist_ok=True)
        path = os.path.join(self.dir,filename)
        df.to_csv(path,index=False)






