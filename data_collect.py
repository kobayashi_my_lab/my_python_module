#学習するデータとテストするデータのカラム数が等しいかどうか
import pandas as pd
import numpy as np
import time

def load_data(PATH):
    """

    読めない時ように例外処理を加える
    いづれloggerに切り替える。

    """
    try:
        print("-----pd.read_csv-----")
        df = pd.read_csv(PATH)
        # df = pd.read_excel(PATH)
        print("load collect!!")
        return df
    except:
        print("-----pd.read_excel-----")
        try:
            df = pd.read_excel(PATH)
            print("load collectl!!")
            return df
        except:
            print("we have to format data or PATH is wrong")



def data_show(data):
    print("-----data information-----")
    print(data.info())
    print("-----data 5 Top-----")
    print(data.head())
    print("-----data describe-----")
    print(data.describe())
    print("-----data isna sum-----")
    print(data.isnull().sum())
    print("-----data unique------")
    print(data.nunique())
    print("-----data shape-----")
    print(data.shape)

def train_test_zero_uniques(TRAIN_PATH,TEST_PATH):
    train_df = load_data(TRAIN_PATH)
    test_df = load_data(TEST_PATH)

    for df in [train_df,test_df]:
        uniques_zero = []
        for col in df.columns:
            uniques = df[col].nunique()
            if uniques == 0:
                uniques_zero.append(col)
                del df[col]
        else:
            print("----- data zero uniques columns------")
            print(uniques_zero)
            print(df.shape)

    else:
        if train_df.shape[1] == test_df.shape[1]:
            print("-----data collumns is collectly!-----")

def reduce_mem_usage(df, verbose=True,date = True):
    start = time()
    print("reduce_mem...")
    numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    start_mem = df.memory_usage().sum() / 1024**2
    for col in df.columns:
        col_type = df[col].dtypes
        if col_type in numerics:
            c_min = df[col].min()
            c_max = df[col].max()
            if str(col_type)[:3] == 'int':
                if c_min > np.iinfo(np.int8).min and c_max < np.iinfo(np.int8).max:
                    df[col] = df[col].astype(np.int8)
                elif c_min > np.iinfo(np.int16).min and c_max < np.iinfo(np.int16).max:
                    df[col] = df[col].astype(np.int16)
                elif c_min > np.iinfo(np.int32).min and c_max < np.iinfo(np.int32).max:
                    df[col] = df[col].astype(np.int32)
                elif c_min > np.iinfo(np.int64).min and c_max < np.iinfo(np.int64).max:
                    df[col] = df[col].astype(np.int64)
            else:
                if c_min > np.finfo(np.float16).min and c_max < np.finfo(np.float16).max:
                    df[col] = df[col].astype(np.float16)
                elif c_min > np.finfo(np.float32).min and c_max < np.finfo(np.float32).max:
                    df[col] = df[col].astype(np.float32)
                else:
                    df[col] = df[col].astype(np.float64)