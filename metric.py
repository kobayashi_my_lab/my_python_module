from sklearn.metrics import roc_auc_score,f1_score,log_loss,accuracy_score
import numpy as np
import pandas as pd

def binary_metric(y_true,probability):
    auc_score = roc_auc_score(y_true,probability)
    loss = log_loss(y_true,probability)
    probability_label = np.where(probability>0.5,1,0)
    accuracy = accuracy_score(y_true,probability_label)
    f_score = f1_score(y_true,probability_label)

    metric_df = pd.DataFrame([auc_score,loss,accuracy,f_score],
                             columns = ["score"],
                             index = ["auc_score","log_loss","accuracy_score","f_score"]
                             )

    return metric_df

def reg_metrric(y_true,y_pred):
    loss_squared = np.sqrt(median_absolute_error(y_true,y_pred))
    loss_abs_squared = median_absolute_error(y_true,y_pred)
    
    metric_df = pd.DataFrame([loss_squared,loss_abs_squared],
                             columns=["score"],
                             index=["mean_squred","abs_mean_squred"]
                             )
    return metric_df

