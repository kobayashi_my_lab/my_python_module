import os,sys
sys.path.append(os.getcwd())
from sklearn.datasets import load_boston
import pandas as pd
from Process import DataProcess
from visualize import DataVisualize

if __name__ == "__main__":
    from sklearn.datasets import load_breast_cancer

    cancer = load_breast_cancer()
    data = cancer.data
    y = cancer.target
    df = pd.DataFrame(data, columns=cancer.feature_names)
    visu = DataVisualize(df,target=y,targetname="target")
    # visu.barplot(x = ["mean radius"],y = "target")
    # visu.scatterplot(x = ["mean radius","mean texture","mean area"],y = "mean area",hue="target")
    # visu.histplot(x = ["mean radius","mean texture"])
    # visu.heatplot(x = ["mean radius","mean texture","mean area","mean compactness","mean concavity"],mask = True)