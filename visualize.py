import seaborn as sns
import matplotlib.pyplot as plt
import os,sys
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from my_python_module.Process import DataProcess
from my_python_module.path_setting import *

def xgb_feature_importance(x,y):
    X_train,X_test,y_train,y_test = train_test_split(x,y,random_state=777)

    unique, count = np.unique(y_train, return_counts=True)
    y_sample_weight = dict(zip(unique, count))
    sample_pos_weight = y_sample_weight[0] / y_sample_weight[1]
    print('pos/neg samples:', y_sample_weight)

    params = {
        "scale_pos_weight":sample_pos_weight,
        "learning_rate" : 0.5,
        "max_depth" :6,
        "n_estimators":500
    }
    folds = KFold(n_splits=5,random_state=1,shuffle=True)
    feature_importances = pd.DataFrame()
    df = pd.DataFrame()
    train = x.values
    for i,(train_idx,test_idx) in enumerate(folds.split(train,y)):
        x_train,x_test = train[train_idx],train[test_idx]
        y_train,y_test = y[train_idx],y[test_idx]
        params["random_state"] = i


        model = xgb.XGBClassifier(**params)
        model.fit(x_train, y_train, verbose=50
                      ,early_stopping_rounds=20, eval_metric="auc",
                      eval_set=[(x_test, y_test)])


        df["feature_importance"] = model.feature_importances_
        df["features"] = x.columns
        df["folds"] = 1+i
        feature_importances = pd.concat([feature_importances,df],axis = 0)

    return feature_importances

def xgb_plot(x,y,name="xgb"):
    #重要な特徴量を保存する


    feature_importance = xgb_feature_importance(x,y)
    order = feature_importance.groupby('features').sum()[['feature_importance']].sort_values('feature_importance',
                                                                                             ascending=False).index
    fig = plt.figure(figsize=(len(feature_importance.columns) * .4, 7))
    ax = fig.add_subplot(111)
    sns.barplot(y="feature_importance", x="features", data=feature_importance,ax = ax,errwidth=1.0,order=order)
    plt.xticks(rotation=90)
    plt.xlabel("特徴量", fontsize=17)
    plt.ylabel("特徴量の重要性", fontsize=17)
    plt.tight_layout()
    plt.savefig(name + '.png')


class DataVisualize(DataProcess):
    def __init__(self,df,target = None,targetname = "target",dir = None ):
        super(DataVisualize,self).__init__(df)
        self.target = pd.DataFrame(target,columns = [targetname])
        self.DataFrame[targetname] = target
        self.dir = self.visualize


    def top5(self,x,y):
        #ターゲットであるyが回帰の時
        #xはカテゴリカルなデータとなる
        grouped = self.DataFrame.groupby(x)[y].mean()
        counts = pd.DataFrame(self.DataFrame.value_counts(ascending = False))
        path =  os.path.join(self.dir,"nunique")
        filename = "uniques_{}.csv".format(x)
        self.save(df = counts,path = path,filename=filename)

        return grouped.head(5)



    def plotfig(self,fig,title,xlabel,ylabel,name,xticks = None):
        plt.title(title, fontsize=17)
        plt.xlabel(xlabel, fontsize=17)
        plt.ylabel(ylabel, fontsize=17)
        if xticks is not None:
            plt.xticks(rotation = 90)
        plt.tight_layout()
        fig.savefig(os.path.join(self.dir, name) + "_{}".format(title) + '.png')
        plt.close(fig)

    #barplot
    def barplot(self,x,y = None,figsize = (12,12),xlabels = ["features"],
                ylabels = ["values"],MethodGrouped = None,name = "barplot"):

        """

        :param x: 連続値
        :param y: カテゴリカルなデータ
        :param figsize　外で決めるときに使う
        :param xlabel:　
        :param ylabel:
        :param groupedXGB:　XGBで簡易的に特徴量を見たいときに使用
        :param title: 画像のタイトル
        :param name:　保存するときの名前
        :return:

        """
        if MethodGrouped is "XGB":
            df = xgb_feature_importance(self.DataFrame.drop([y],axis = 1),self.DataFrame[y])

            y = "features"
            x = ["feature_importance"]
            max_num_features = min(len(df[y]), 40)
            order = df.groupby(y).sum()[["feature_importance"]].sort_values(
                "feature_importance",
                ascending=False)[:max_num_features].index
            xticks = True

        elif MethodGrouped is "top5":
            for category in x:

                df = self.top5(x = category,y = y)
                fig = plt.figure(figsize=figsize)
                ax = fig.add_subplot(111)
                df.plot.bar(ax = ax)
                self.plotfig(fig = fig,title = category,xlabel = "features",ylabel = "values",name=name)
            return None

        else:
            df = self.DataFrame
            xticks = None
            order = None



        if (len(x) != len(xlabels)) and (len(y) != len(ylabels)):
            xlabels = xlabels * len(x)
            ylabels = ylabels * len(x)


        for i,(col,xlabel,ylabel) in enumerate(zip(x,xlabels,ylabels)):

            if (figsize is None ) and (MethodGrouped is not None):
                fig = plt.figure(figsize=(len(df.columns) * 4.4, 7))
            else:
                fig = plt.figure(figsize=figsize)

            ax = fig.add_subplot(111)
            sns.barplot(x=y,y=col, data=df, ax=ax, errwidth=1.0, order=order)
            self.plotfig(fig = fig,title = col,xlabel = xlabel,ylabel = ylabel,name = name,xticks = xticks)




    #scatterplot
    def scatterplot(self,x,y = None,hue = None,figsize = None,name = "scatter",pair = None):
        """

        :param self:
        :param x: x軸の分散 リスト
        :param y: y軸の分散
        :param figsize:
        :param name:
        :param hue:カテゴリカルなデータ　色付け
        :return:
        """
        df = self.DataFrame
        if pair is None:
            for i,valx in enumerate(x):
                if figsize is None:
                    fig = plt.figure(figsize=(12, 12))
                ax = fig.add_subplot(111)
                plt.xlim(np.percentile(self.DataFrame[valx],[10]),np.percentile(self.DataFrame[valx],[90]))
                plt.ylim(np.percentile(self.DataFrame[y],[10]),np.percentile(self.DataFrame[y],[90]))
                sns.scatterplot(x=valx, y=y, data=df, ax=ax, hue = hue)
                self.plotfig(fig=fig, title=valx, xlabel=valx, ylabel=y, name=name)

        else:
            height = len(x) * 1.4
            fig = sns.pairplot(data=df,vars = x,hue = hue,kind="scatter",
                               height=height,palette="husl")
            plt.tight_layout()
            fig.savefig(os.path.join(self.dir, name) + "_pair" + '.png')

    #histplot
    def histplot(self,x,hue = None,Facet = None,name = "hist"):

        """

        :param x: ヒストグラムで視覚化する特徴量
        :param hue: カテゴリによる分別
        :param Facet: default None Trueにすると分別
        :param name:
        :return:
        """

        df = self.DataFrame
        if Facet is  None:
            self.outlinear(columns=x,plot = True,name = name)
        else:
            for col in x:
                height = df["target"].nunique() * 1.8
                bins = int(np.log2(len(df[col]) + 1))
                fig = sns.FacetGrid(data=df, col=hue, hue=hue, height=height)
                fig = fig.map(sns.distplot, col, bins=bins, kde=True)
                fig.savefig(os.path.join(self.dir,name)+"_{}".format(col)+".png")


    #heatmap
    def heatplot(self,x,figsize = None,mask = None,name = "heatmap"
                 ,xlabel = "features",ylabel = "std",title = "heatmap"):
        """
        heatmap を描きます。
        :param x: 相関係数を求めたい特徴量
        :param figsize:
        :param mask: 対称行列を消去 default None
        :param name:
        :param xlabel:
        :param ylabel:
        :param title:
        :return:
        """

        df = self.DataFrame
        if figsize is None:
            fig = plt.figure(figsize=(len(x) * 4.4, 10))
        else:
            fig = plt.figure(figsize=figsize)

        corr = df[x].corr()
        if mask is not None:
            mask = np.zeros_like(corr)
            mask[np.triu_indices_from(mask)] = True

        ax = fig.add_subplot(111)
        sns.heatmap(corr, annot=True, square=True, mask=mask, fmt="1.3f",
                          cmap="Blues", linewidths=0.5,ax = ax)
        self.plotfig(fig = fig, title = title, xlabel = xlabel, ylabel = ylabel, name = name)


    #lineplot























